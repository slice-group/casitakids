class Article < ActiveRecord::Base
	belongs_to :order
	belongs_to :kid
	belongs_to :symphony
	validates_presence_of :kid_id, :disc_name, :disc_message, :symphony_id
	validates_length_of :disc_name, :minimum => 5, :maximum => 15
 	validates_length_of :disc_message, :minimum => 5, :maximum => 40, :allow_blank => true
 	before_validation :add_kid_id
 	validate :name_kid_exists
 	
 	def name_kid_exists
    unless Kid.exists? self.kid_name
    	errors.add(:kid_name, "If the name doesn't exists, you won't be able to order") 
    end
  end

  def add_kid_id
  	self.kid_id = Kid.find_by_name(self.kid_name).id if Kid.exists? self.kid_name
  end
end
