#Generado por keppler
require 'elasticsearch/model'
class Symphony < ActiveRecord::Base
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks
  mount_uploader :cover, CoverUploader
  mount_uploader :song, SongUploader
  has_many :articles
  validates_presence_of :cover, :song, :title, :description, :age, :price
  validates :price, numericality: { greater_than_or_equal_to: 1 }

  
  after_commit on: [:update] do
    __elasticsearch__.index_document
  end
  
  def self.searching(query)
    if query
      self.search(self.query query).records.order(id: :desc)
    else
      self.order(id: :desc)
    end
  end

  def self.query(query)
    { query: { multi_match:  { query: query, fields: [:id, :price, :title] , operator: :and }  }, sort: { id: "desc" }, size: self.count }
  end

  #armar indexado de elasticserch
  def as_indexed_json(options={})
    {
      id: self.id.to_s,
      price:  self.price.to_s,
      title:  self.title
    }.as_json
  end

  def color
    case self.id
      when 1
        "primary"
      when 2
        "green"
      when 3
        "blue"
      else
        ["primary", "green", "blue"].sample
    end
  end

end
#Symphony.import
