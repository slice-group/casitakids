class Kid < ActiveRecord::Base
	has_many :articles
	validates_presence_of :name
	validates_uniqueness_of :name

	def mp3(symphony)
		case symphony.to_i
			when 1
				"#{self.name.gsub(" ", "").humanize}_sinfonia1_reel.mp3"
			when 2
				"#{self.name.gsub(" ", "").humanize}_Sinfonia2_Reel_01.mp3"
			when 3
				"#{self.name.gsub(" ", "").downcase}_Sinfonia3_Reel_01.mp3"
			else
				nil
		end
	end

	def self.exists?(name)
		!Kid.find_by_name(name).nil?
	end
end
