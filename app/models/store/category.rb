module Store
  class Category < ActiveRecord::Base
    establish_connection :store_database_development
    self.table_name = 'categories'
    has_many :products
  end
end
