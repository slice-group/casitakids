module Store
  class Product < ActiveRecord::Base
    establish_connection :store_database_development
    self.table_name = 'products'
    include ApplicationHelper
    belongs_to :category
    has_many :product_images

    def self.all_publics
      all.order('category_id asc')
    end

    def color
      category.disc_check ? :primary : :green
    end

    def image_active
      image_active = product_images.select(&:principal?)
      "#{store_path}#{image_active.first.external_image}"
    end
  end
end
