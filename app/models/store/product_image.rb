module Store
  class ProductImage < ActiveRecord::Base
    establish_connection :store_database_development
    self.table_name = 'product_images'
    belongs_to :product
  end
end
