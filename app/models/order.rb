#Generado por keppler
require 'elasticsearch/model'
class Order < ActiveRecord::Base
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks
  has_many :articles
  accepts_nested_attributes_for :articles, reject_if: :all_blank, allow_destroy: true
  validates_presence_of :name, :email, :phone
  validates :phone, numericality: { only_integer: true }
  validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i }
  
  after_commit on: [:update] do
    __elasticsearch__.index_document
  end
  
  def self.searching(query)
    if query
      self.search(self.query query).records.order(id: :desc)
    else
      self.order(id: :desc)
    end
  end

  def self.query(query)
    { query: { multi_match:  { query: query, fields: [:id, :kid_name, :symphony, :quantity, :name, :email, :phone] , operator: :and }  }, sort: { id: "desc" }, size: self.count }
  end

  #armar indexado de elasticserch
  def as_indexed_json(options={})
    {
      id: self.id.to_s,
      name: self.name,
      email: self.email,
      phone: self.phone
    }.as_json
  end

end
#Order.import
