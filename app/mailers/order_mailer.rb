class OrderMailer < ApplicationMailer
	def notification_client(order)
		@order = order
		mail(from: "information@casitakids.com", to: "#{@order.email}", subject: "#{@order.email} Your order has been received")
	end

	def notification_admin(order)
		@order = order
		mail(from: "information@casitakids.com", to: "casitakids@hotmail.com", subject: "You have received an order from #{@order.email}")
	end
end
