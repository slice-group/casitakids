class FrontendController < ApplicationController
  layout 'layouts/frontend/application'
  autocomplete :kid, :name

  def index
    @symphonies = Symphony.where(public: true)
  end

  def about
  end

  def our_mision
  end

  def our_products
  end

  def contact_us
    @message = KepplerContactUs::Message.new
  end

  def hear
    @symphony = Symphony.find(params[:symphony_id])
    @kid = Kid.find_by_name(params[:kid_name])

    respond_to do |format|
      format.js
    end
  end

  def new_order
    @symphony = Symphony.find(params[:symphony_id])
  end

  def create_order
    @symphonies = Symphony.where(public: true).order(:title)
    @order = Order.new(order_params)

    if @order.save
      OrderMailer.notification_client(@order).deliver_now
      OrderMailer.notification_admin(@order).deliver_now
      redirect_to root_path, notice: 'We have received your order, we will soon be communicating with you.'
    else
      flash[:alert] = 'There was an error in filling out the order, please check the entered data'
      render :new_order
    end
  end

  private

  def order_params
    params.require(:order).permit(:refence, :name, :email, :phone, articles_attributes: [:kid_id, :kid_name, :symphony_id, :disc_name, :disc_message, :_destroy])
  end
end
