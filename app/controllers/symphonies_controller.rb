#Generado con Keppler.
class SymphoniesController < ApplicationController
  before_filter :authenticate_user!
  layout 'admin/application'
  load_and_authorize_resource
  before_action :set_symphony, only: [:show, :edit, :update, :destroy]

  # GET /symphonies
  def index
    symphonies = Symphony.searching(@query).all
    @objects, @total = symphonies.page(@current_page), symphonies.size
    redirect_to symphonies_path(page: @current_page.to_i.pred, search: @query) if !@objects.first_page? and @objects.size.zero?
  end

  # GET /symphonies/1
  def show
  end

  # GET /symphonies/new
  def new
    @symphony = Symphony.new
  end

  # GET /symphonies/1/edit
  def edit
  end

  # POST /symphonies
  def create
    @symphony = Symphony.new(symphony_params)

    if @symphony.save
      redirect_to @symphony, notice: 'Symphony was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /symphonies/1
  def update
    if @symphony.update(symphony_params)
      redirect_to @symphony, notice: 'Sinfonía actualizada satisfactoriamente.'
    else
      render :edit
    end
  end

  # DELETE /symphonies/1
  def destroy
    @symphony.destroy
    redirect_to symphonies_url, notice: 'Symphony was successfully destroyed.'
  end

  def destroy_multiple
    Symphony.destroy redefine_ids(params[:multiple_ids])
    redirect_to symphonies_path(page: @current_page, search: @query), notice: "Usuarios eliminados satisfactoriamente"
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_symphony
      @symphony = Symphony.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def symphony_params
      params.require(:symphony).permit(:cover, :price, :title, :description, :song, :age, :public, :store_url)
    end
end
