# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
#user = CreateAdminService.new.call
#puts 'CREATED ADMIN USER: ' << user.email

[:admin].each do |name|
	Role.create name: name
	puts "#{name} creado"
end

User.create name: "Admin", email: "admin@inyxtech.com", password: "12345678", password_confirmation: "12345678", role_ids: "1"
puts "admin@inyxtech.com ha sido creado"



#--------------------name of girls----------------------------------

#name for A
["Abby","Abril","Ada","Adel","Adela","Adelaida","Adira","Adriana","Aerin","Africa","Agatha","Agostina","Agustina","Aida","Aidil","Aileen","Ailen","Aimara","Aime","Aina","Ainara","Ainhoa","Aitana","Aixa","Alaia","Alana","Alba","Aldana","Alegra","Alegria","Alejandra","Alessandra","Alessia","Alexa","Alexandra","Alexia","Alfonsina","Alicia","Alina","Alis","Alissa","Allison","Alma","Almendra","Almudena","Alondra","Aluna","Amaia","Amalia","Amanda","Amaranta","Amarjot","Amaya","Ambar","Amelia", "Amelie","Ameth","Amina","Amparo","Amy","Ana","Ana Amelia","Ana Belen","Ana Daniela","Ana Gaby","Ana Isabel","Ana Jose","Ana Lia","Ana Lucia","Ana Maria","Ana Paola","Ana Patricia","Ana Paula","Ana Victoria","Anabel","Anabella","Anahi","Anais","Anaridis","Anasofia","Anasol","Anastasia","Andrea","Andreina","Andy","Ane","Angel Maria","Angela","Angeles","Angelica","Angelina","Angelique","Angely","Angie","Angie lizbeth","Ani","Ania","Anika","Ann","Annais","Annette","Antonella","Antonia","Antonieta","April","Araceli","Aranza","Arcelia","Arely","Ariadna","Ariana","Arianny","Arlet","Aroa","Ashanty", "Ashley","Astrid","Asun","Asuncion","Audrey","Augusta","Aura","Aurelia","Aurora","Ava","Aya","Ayelen","Aymar","Azucena","Azul"].each { |name| Kid.create name: name }

#name for B
["Balbina","Barbara","Bea","Beatriz","Becky","Begona","Belem" ,"Belen","Belinda","Berenice","Bernarda","Bernardita" ,"Berni","Bertha","Betina","Betty Sofia","Bia","Bianca","Blanca","Brenda","Briana","Brisa","Brittany","Bruna","Brunella"].each { |name| Kid.create name: name }

#name for C
["Cai","Calissa","Camila","Camille","Candela","Candelaria","Caridad","Carlota","Carmela","Carmen","Carmina","Carol","Carola","Carolina","Caroline","Casandra","Cassilda","Catalina","Cayetana","Cecilia","Celeste","Celia","Celina","Celine","Chana","Channel","Chantal","Chantelle","Charlotte","Chi","Chus","Ciara","Cielo","Cindy","Claire","Clara","Clarisa","Claudia","Claudin","Clementina","Cloe","Colomba","Concepcion","Concha","Constanza","Consuelo","Coral","Corina","Cossette","Cristal","Cristie","Cristina","Cynthia"].each { |name| Kid.create name: name }

#name for D
["Dafne","Daira","Daleiby","Dana","Danae","Daniela","Danna","Dariana","Dasha","Dayana","Delfina","Delia","Denisse","Desiree","Deva","Diana","Dina","Diosa","Dolo","Dolores","Domenica","Dominga","Dominic","Dora","Dulce","Dulce Maria"].each { |name| Kid.create name: name }

#name for E
["Ela","Elba","Eleonor","Eli","Elia","Eliana","Elianni","Elina","Elisa","Elisabeth","Elma","Eloisa","Eloise","Elsa","Elvira","Emanuela","Emanuella","Emilia","Emiliana","Emilse","Emily","Emma","Emma Sofia","Encarna" ,"Erika","Espe","Esperanza","Estefania","Estefi","Estela","Esther","Estivaliz","Estrella","Eugenia","Eulalia","Eva","Eva Maria","Evangelina","Evelin"].each { |name| Kid.create name: name }

#name for F
["Fabiana","Fabiola","Fanny","Farah","Fatima","Fatima","Fe","Federica","Fedora","Felicia","Felicitas","Felisa","Fernanda","Fiamma","Fiona","Fiorela","Flavia","Flor","Flora","Florencia","Florentina","Fong","Francesca","Francisca","Frania","Frida"].each { |name| Kid.create name: name }

#name for G
["Gabriela","Gaia","Gaitana","Gala","Galia","Galilea","Gema" ,"Genesis","Genoveva","Georgina","Geraldine","Getsemani","Gia","Giada","Gianella","Giannina","Gina","Giovanna","Gisela","Gisella","Giselle","Giulia","Giuliana","Gloria","Gloriana","Grace","Gracia","Graciela","Gretel","Guadalupe","Guillermina"].each { |name| Kid.create name: name }

#name for H
["Hanna","Hannia","Hasbleidy","Heidy","Helen","Helena","Helena Maria","Hepsie","Heydy","Hillary","Hiroki"].each { |name| Kid.create name: name }

#name for I
["Idoia","Ignacia","Ilana","Iliana","Ilona","Indiana","Ines","Inessa","Ingrid","Inma","Inmaculada","Inti","Irene","Iria","Irina","Iris","Isa","Isabel","Isabela","Isadora" ,"Isidora","Isis","Itzel","Itziar","Ivanna","Ivonne"].each { |name| Kid.create name: name }

#name for J
["Jacinta","Jackie","Jacqueline","Jade","Janner","Jaqueline","Javara","Javiera","Jazmin","Jeannette","Jehily","Jenifer","Jennifer","Jenny","Jessica","Jimena","Joanna","Joaquina","Jocelyn","Johanna","Jondyra","Josefa","Josefina","Juana","Juanita","Judith","Julia","Juliana","Julie","Julieta"].each { |name| Kid.create name: name }

#name for K
["Karen","Karin","Karina","Karla","Katerine","Katherina","Keily","Keisy","Kiara","Kim","Kimberly","Kora","Krisna","Kristel","Kumari"].each { |name| Kid.create name: name }

#name for L
["Laia","Laila","Lara","Larisa","Laura","Laura Sofia","Leah","Leidy","Leila","Leire","Lena","Lenis","Lenny","Leonela","Leonor","Leonora","Leticia","Lia","Lia Mari","Liat","Lidia","Lil Amelia","Lila","Lilian","Liliana","Lily","Lina","Lina Sofia","Linda","Liz","Liz Gabriela","Liza","Lizbeth","Lluvia","Loana","Lola","Loles","Lorany","Loredana","Lorely","Lorena","Lorenza","Loreto","Lourdes","Lua","Luana","Lucero","Lucia","Luciana","Lucianne","Lucila","Lucille","Lucrecia","Lucy","Ludmila","Luisa","Luisa Fernanda","Luisa Maria","Luisana","Luisina","Luna","Lupe","Lupita","Luz","Luz Angela","Lyra"].each { |name| Kid.create name: name }

#name for M
["Mabel","Macarena","Maciel","Madeleine","Madison","Maelle","Magali","Magda" ,"Magdalena","Magnolia","Maia","Maika","Mailen","Maina","Maira","Malak","Malena","Mallory","Mamen" ,"Mane","Manola","Manuela","Mar","Mara","Marcela","Marene","Marga","Margareth","Margarita","Margo","Mari Angeles","Mari Fe","Mari Luz","Mari Mar","Mari Nieves","Maria","Maria Alejandra","Maria Amparo","Maria Andrea","Maria Antonia","Maria Antonieta","Maria Aranzazu","Maria Aurora","Maria Beatriz","Maria Begona","Maria Belen","Maria Camila","Maria Celeste ","Maria Clara","Maria Concepcion","Maria Cristina","Maria de la Paz","Maria de los Angeles ","Maria del Mar","Maria Dolores","Maria Eugenia","Maria Fernanda","Maria Gabriela","Maria gracia","Maria Guadalupe","Maria Helena","Maria Ignacia","Maria Isabel","Maria Jesus","Maria Joaquina","Maria Jose","Maria Josefa","Maria Juana","Maria Julia","Maria Juliana","Maria Laura","Maria Lucia","Maria Luisa","Maria Mercedes","Maria Paula","Maria Paulina","Maria Pilar","Maria Rocio","Maria Rosa","Maria Sofia","Maria Soledad","Maria Teresa","Maria Valentina","Maria Valeria","Maria Victoria","Marian","Mariana","Mariangel","Mariangeles","Mariapaz","Mariapia","Maribel","Maricarmen","Marie","Mariel","Mariela","Marina","Mariona","Maripaz","Marirreme","Marisa" ,"Marisela","Marisol","Marlene","Marta","Martina","Mary","Matilda","Matilde","Maui","Maya","Mayelin","Mayri","Mayte","Megan","Melanie","Melina","Melisa","Merce","Mercedes","Merche","Mia","Micaela","Michelle","Miguelina","Mila","Milagros","Milena","Millaray","Mimmi","Miranda","Mireia","Miren","Miriam","Misaki","Monica","Monserrat","Montana","Montse","Mora","Morena"].each { |name| Kid.create name: name }

#name for N
["Nabila","Nadia","Nahiara","Naia","Naiara","Namaste","Naomi","Naroa","Nashla","Natalia","Nataly","Natasha","Natividad","Nayeli","Nayura","Nazarena","Neela","Nerea","Nessie","Neus","Nicole","Nidia","Nieves","Nina","Noel","Noelia","Noemi","Noenyi","Nora","Norma","Nuria"].each { |name| Kid.create name: name }

#name for O
["Olga","Olivia","Oriana","Ornella"].each { |name| Kid.create name: name }

#name for P
["Paloma","Pamela","Paola","Pascal","Pastora","Patricia","Paula","Paulette","Paulina","Paz","Penelope","Perla","Phoebe","Pia","Pierina","Pilar","Piluca","Priscila","Puri","Purificacion","Rachel","Rafaela","Ramona","Raquel"].each { |name| Kid.create name: name }

#name for R
["Rayen","Rebecca","Regina","Reme","Renata","Rio","Rita","Roberta","Rocio","Romina","Rosa","Rosa Mari","Rosa Maria","Rosalia","Rosanna","Rosario","Rose","Rosita","Roxanna","Ruth"].each { |name| Kid.create name: name }

#name for S
["Saadia","Sabina","Sabrina","Sadot","Salma","Salome","Samanta","Samara","Sandra","Sara","Sarai","Sarita","Sasha","Savana","Scarlet","Selena","Shanon","Sharlin","Sharon","Sheila","Shenoa","Siena","Sigal","Sigrid","Silvana","Silvia","Simona","Simone","Sinthia","Sobeida","Sofia","Sol","Solana","Solange","Sole","Soledad","Sonali","Sonia","Sophie","Soraya","Stacy","Stephanie","Suri","Susan","Susana","Suzette","Sydney"].each { |name| Kid.create name: name }

#name for T
["Talia","Taliana","Tamara","Tania","Tatiana","Tea","Telma","Tere","Teresa","Tiare","Tifany","Tinsae","Tiziana","Trinidad","Tulia"].each { |name| Kid.create name: name }

#name for U
["Uma" ,"Ursula"].each { |name| Kid.create name: name }

#name for V
["Valentina","Valeria","Valery","Valeska","Valle","Vanesa","Vania","Vanina","Vashu","Vega","Venus","Vera","Vera Sofia","Verena","Veronica","Vicenta","Victoria","Vida","Violet","Violeta","Virginia","Vitoria" ,"Vivian","Viviana"].each { |name| Kid.create name: name }

#name for X
["Xana","Xiomara","Xochitl"].each { |name| Kid.create name: name }

#name for Y
["Yaiza","Yamila","Yamile","Yanina","Yaresky","Yaretzi","Yazmin","Yolanda"].each { |name| Kid.create name: name }

#name for Z
["Zahara","Zaid","Zaida","Zaira","Zamira","Zoe","Zulema","Zulma"].each { |name| Kid.create name: name }


puts "names girls complete"

#--------------------name of boys----------------------------------

#name for A
["Aaron","Abel","Abiel","Abigail","Abraham","Adam","Adolfo","Adria""Adrian","Agapito","Agustin","Aiden","Aitor","Akari","Alan","Albert","Alberto","Aldo","Alec","Aleix","Alejandro","Alejo","Alessandro","Alessio","Alex","Alexander","Alexandro" ,"Alexis","Alfonso","Alfredo","Alido","Alonso","Alvaro","Amadito","Amador","Amaro","Amaru","Amir","Ander","Anderson","Andoni","Andre","Andres","Andres Felipe","Andreu","Andrew","Andrey","Angel","Angel Esteban","Angelo","Anibal","Anthony","Anton","Antonio","Antuan","Apolo","Aquiles","Argemiro","Arie","Ariel","Armando","Arnau","Arnold","Artemio","Arthur","Arturo","Asier","Augusto","Aureliano","Aurelio","Austin","Axel","Ayrton","Aziz"].each { |name| Kid.create name: name }

#name for B
["Baltazar","Bastian","Bastian","Bautista","Beltran","Benicio","Benito","Benjamin","Bernabe","Bernardo","Biel","Bienvenido","Bilal","Blake","Blas","Boris","Borja","Brandon","Braulio","Brenan","Bruno","Bryan","Byron"].each { |name| Kid.create name: name }

#name for C
["Caetano","Caleb","Camilo","Carles","Carlos","Carlos Andres","Carlos Daniel","Carlos David","Catriel","Cayetano","Cesar","Cesar Augusto","Charlie","Chau","Chema","Chen","Christien","Christofer","Cipriano","Cirilo","Ciro","Clark","Claudio","Clemente","Constantino","Crescente","Cristian","Cristiano","Cristobal","Cuauhtemoc","Curro"].each { |name| Kid.create name: name }

#name for D
["Damian","Dani","Daniel","Daniel Fernando","Danilo","Dante","Dariel","Dario","David","Davor","Demian","Derek","Diego","Diego Alejandro","Dimas","Dimitri","Domenico","Domingo","Dominic","Dorimar","Douglas","Dylan"].each { |name| Kid.create name: name }

#name for E
["Eddy","Edgar","Edilson","Edson","Eduardo","Edward","Edwin","Efren","Egmont","Eleazar","Eleuterio","Elian","Elias","Elias David","Eliel","Elinoi","Elkin","Eloy","Emanuel","Emil","Emiliano","Emilio","Eneco","Enrique","Enubio","Enzo","Eric","Ernesto","Erwing","Esteban","Estefano","Estuardo","Ethan","Ethian","Eugenio","Evan","Ezequiel"].each { |name| Kid.create name: name }

#name for F
["Francesc","Francesco","Francisco","Francisco Javier","Francisco Jose","Franco","Franklin","Franz" ,"Freddy","Fabian","Fabiano","Fabio","Fabricio","Facundo","Farid","Fausto","Federico","Felipe","Felix","Fermin","Fernan","Fernando","Fernando Jose","Ferran","Fidel","Fidias" ,"Filiberto","Flavio" ,"Flenix"].each { |name| Kid.create name: name }

#name for G
["Gabriel","Gael","Gamal","Gaspar","Gaston","Genaro","Gerald","Gerard","Gerardo","German","Gian Franco","Gian Marco","Giancarlos","Gianluca","Gianmanuel","Gil","Gilberto","Gines","Gino","Giovanny","Gonzalo","Gorka","Goyo","Gregorio","Guido","Guillem","Guillermo","Gustavo"].each { |name| Kid.create name: name }

#name for H
["Hansel","Harry","Hector","Henry","Hernan","Hernando","Hipolito","Horacio","Huascar","Hugo","Humberto"].each { |name| Kid.create name: name }

#name for I
["Iago","Ian","Ian","Iato","Ignacio","Igor","Iker","Ilan","Imanol","Inaki","Iñigo","Isaac","Isai","Isaias","Isaio","Isidro","Ismael","Israel","Italo","Ivan","Ives"].each { |name| Kid.create name: name }

#name for H
["Jacinto","Jack","Jackson","Jacob","Jacob","Jacobo","Jaiden","Jaime","Jair","Jairo","James","Jan","Janiel","Jared","Jason","Jaume","Javier","Javier Ignacio","Jayden","Jean Daniel","Jean Paul","Jean Pierre","Jefferson","Jefry","Jeremy","Jeriel","Jeronimo","Jesus","Jhon Freddy","Jhonatan","Jhonatan","Jimi","Joan","Joaquim","Joaquin","Joel","Johan","John","Johnny","Jonas","Jordi","Jorge","Jorge Andres","Jorge Antonio","Jorge Ivan","Jose","Jose Alejandro","Jose Alfredo","Jose Angel","Jose Antonio","Jose Armando","Jose Augusto","Jose Carlos","Jose Daniel","Jose David","Jose Eduardo","Jose Emilio","Jose Francisco","Jose Gabriel","Jose Guadalupe","Jose Guillermo","Jose Ignacio","Jose Ivan" ,"Jose Javier","Jose juan","Jose Julian","Jose Luis","Jose Manuel","Jose Maria","Jose Miguel","Jose Pablo","Jose Pedro","Jose Rafael","Jose Ramon","Jose Ricardo","Jose Roberto","Jose Tomas" ,"Jose Vicente","Joseba","Josep","Josep Maria","Joseph","Joshua","Josue","Juan","Juan Alejandro","Juan Alvaro","Juan Andres","Juan Angel","Juan Antonio","Juan Bautista","Juan Camilo","Juan Carlos","Juan Cristobal","Juan Cruz","Juan David","Juan Diego","Juan Esteban","Juan Felipe","Juan Fernando","Juan Francisco"",""Juan Gabriel","Juan Guillermo","Juan Ignacio","Juan Jacobo","Juan Javier","Juan Jose","Juan Luis","Juan Manuel","Juan Martin","Juan Mateo","Juan Matias","Juan Miguel","Juan Nicolas","Juan Pablo","Juan Patricio","Juan Pedro","Juan Rafael","Juan Ramon","Juan Ricardo","Juan Santiago","Juan Sebastian","Juan Simon","Julen","Julian","Julio","Julio Mario","Junior","Justin","Justo"].each { |name| Kid.create name: name }

#name for K
["Kai","Kalil","Karin","Keiko","Keneth","Kenneth","Kento","Kevin","Keylor","Kian","Kike","Kyler"].each { |name| Kid.create name: name }

#name for L
["Lautaro","Lazaro" ,"Leandro","Lenin","Leo","Leon","Leonardo","Leonel","Leopoldo","Liam","Lionel","Luis","logan","Lorenzo","Luca","Lucas","Luciano","Lucien" ,"Lucienne","Lucio","Luis","Luis Alejandro","Luis Alfonso","Luis Angel","Luis Armando","Luis Carlos","Luis Eduardo","Luis Esteban","Luis Felipe","Luis Fernando","Luis Gabriel","Luis Guillermo","Luis Jose","Luis Miguel"].each { |name| Kid.create name: name }

#name for M
["Maire","Malcolm","Manolo","Manu","Manuel","Manuel Jose","Marcel","Marcelo","Marcio","Marck","Marco","Marcos","Mariano","Mario","Mario Andres","Marlon","Marti","Martin","Martino","Mason","Massimo","Mateo","Mathew","Matia","Matia","Matias","Mauricio","Mauro","Max","Maximilian","Maximiliano","Maximo","Meir","Melvin","Michael","Miguel","Miguel Adrian","Miguel Angel","Mijail","Mijal","Mike","Mikel","Mirko","Misael","Mohamed","Mohini","Moises","Monserrat"].each { |name| Kid.create name: name }

#name for N
["Nahuel","Naoto","Nariko","Nassir","Nathan","Natxo","Nehuen","Nelson","Nessim","Nestor","Nicanor","Nicauri","Nico","Nicolas","Nil","Nino","Nirel","Noah","Noe","Noel","Nur"].each { |name| Kid.create name: name }

#name for O
["Octavio","Oliver","Omar","Oriol","Orlando","Oscar","Oswaldo","Owen"].each { |name| Kid.create name: name }

#name for P
["Pablo","Pablo Esteban","Paco","Panchito","Paolo","Pascual","Pastor" ,"Patricio","Patrick","Pau","Paul","Paulo","Pedro","Pedro Jose","Pedro Juan","Pedro Pablo","Pelayo","Pepe","Pere","Peter","Philip","Philippe","Pietro","Pique"].each { |name| Kid.create name: name }

#name for Q
["Qian"].each { |name| Kid.create name: name }

#name for R
["Rafael","Rainmundo","Ramiro","Ramon","Randy","Raul","Rayan","Reinaldo","Remigio","Renato","Rene","Renzo","Rian","Ricardo","Richard","Rigoberto","Roberth","Roberto","Rodolfo","Rodrigo","Rogelio","Roger","Rohan","Rolando","Roman","Romeo","Ronaldo","Rosendo","Ruben"].each { |name| Kid.create name: name }

#name for S
["Said","Sair","Salim","Salomon","Salva","Salvador","Salvatore","Sam","Samir","Samuel","Sandy","Sandy alexander","Santiago","Santino","Saul","Saulo","Sayan","Sayid","Sebastian","Sebastian","Sebastian","Sergio","Sergy","Silvestre","Silvio","Simon","Simon Pedro","Steven"].each { |name| Kid.create name: name }

#name fo T
["Telmo","Teo","Teodoro","Thiago","Ticiano","Timoteo","Tinin","Tito","Tobias","Tomas","Thomas","Tonatiu","Tristan"].each { |name| Kid.create name: name }

#name for U
["Ulises","Unai","Uriel"].each { |name| Kid.create name: name }

#name for V
["Valdomero","Valentin","Valentino","Valeriano","Vasco","Vicente","Victor","Victor Ivan","Victor Manuel","Vidal"].each { |name| Kid.create name: name }

#name for W
["Walter","Werner","Weslie","Wilfredo","Wilfrido","William","Wilson","Wolter"].each { |name| Kid.create name: name }

#name for X
["Xavi","Xavier"].each { |name| Kid.create name: name }

#name for Y
["Yadiel","Yago","Yako","Yamil","Yan","Yandel","Yared","Yeray","Yirias","Yoel","Yusef"].each { |name| Kid.create name: name }

#name for Z
["Zac"].each { |name| Kid.create name: name }

puts "names boys complete"