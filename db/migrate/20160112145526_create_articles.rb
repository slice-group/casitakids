class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.integer :order_id
      t.string :kid_name
      t.integer :kid_id
      t.integer :symphony_id
      t.string :disc_name
      t.string :disc_message

      t.timestamps null: false
    end
  end
end
