class AddStoreUrlToSymphonies < ActiveRecord::Migration
  def change
    add_column :symphonies, :store_url, :string
  end
end
