class CreateSymphonies < ActiveRecord::Migration
  def change
    create_table :symphonies do |t|
      t.string :cover
      t.float :price
      t.string :title
      t.text :description
      t.string :song
      t.string :age
      t.boolean :public

      t.timestamps null: false
    end
  end
end
