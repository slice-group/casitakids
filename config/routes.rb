Rails.application.routes.draw do

  root to: 'frontend#index'

  get "/about", to: "frontend#about", as: :about
  get "/our-mision", to: "frontend#our_mision", as: :mision
  get "/contact", to: "frontend#contact_us", as: :contact
  get "/order/:symphony_id", to: "frontend#new_order", as: :new_order_frontend
  get "/verify-name", to: "frontend#verify_name", as: :verify_name
  post "/hear", to: "frontend#hear", as: :hear
  post "/order", to: "frontend#create_order", as: :create_order
  get :autocomplete_kid_name, to: "frontend#autocomplete_kid_name", as: :autocomplete_kid_name_orders

  devise_for :users, skip: KepplerConfiguration.skip_module_devise

  resources :admin, only: :index

  scope :admin do

  	resources :users do
      get '(page/:page)', action: :index, on: :collection, as: ''
      delete '/destroy_multiple', action: :destroy_multiple, on: :collection, as: :destroy_multiple
    end

    resources :symphonies do
      get '(page/:page)', action: :index, on: :collection, as: ''
      delete '/destroy_multiple', action: :destroy_multiple, on: :collection, as: :destroy_multiple
    end

    resources :orders do
      get '(page/:page)', action: :index, on: :collection, as: ''
      delete '/destroy_multiple', action: :destroy_multiple, on: :collection, as: :destroy_multiple

    end

  end

  #errors
  match '/403', to: 'errors#not_authorized', via: :all, as: :not_authorized
  match '/404', to: 'errors#not_found', via: :all
  match '/422', to: 'errors#unprocessable', via: :all
  match '/500', to: 'errors#internal_server_error', via: :all


  #dashboard
  mount KepplerGaDashboard::Engine, :at => '/', as: 'dashboard'

  #contact_us
  mount KepplerContactUs::Engine, :at => '/', as: 'messages'


end
