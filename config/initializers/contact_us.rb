# Agregar datos de configuración
KepplerContactUs.setup do |config|
	config.mailer_to = "information@casitakids.com"
	config.mailer_from = "information@casitakids.com"
	config.name_web = "Casita Kids"
	#Route redirection after send
	config.redirection = "/contact"


	# Agregar keys de google recaptcha
	Recaptcha.configure do |config|
	  config.public_key  = "6LeCbRUTAAAAAN-vDoM19Cebyt9n1C-khzk2ACVr"
	  config.private_key = "6LeCbRUTAAAAAKMJHoXLiRDbPDRIdNiXeqVYs6WD"
	end
end