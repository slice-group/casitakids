require 'test_helper'

class SymphoniesControllerTest < ActionController::TestCase
  setup do
    @symphony = symphonies(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:symphonies)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create symphony" do
    assert_difference('Symphony.count') do
      post :create, symphony: { age: @symphony.age, cover: @symphony.cover, description: @symphony.description, price: @symphony.price, song: @symphony.song, title: @symphony.title }
    end

    assert_redirected_to symphony_path(assigns(:symphony))
  end

  test "should show symphony" do
    get :show, id: @symphony
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @symphony
    assert_response :success
  end

  test "should update symphony" do
    patch :update, id: @symphony, symphony: { age: @symphony.age, cover: @symphony.cover, description: @symphony.description, price: @symphony.price, song: @symphony.song, title: @symphony.title }
    assert_redirected_to symphony_path(assigns(:symphony))
  end

  test "should destroy symphony" do
    assert_difference('Symphony.count', -1) do
      delete :destroy, id: @symphony
    end

    assert_redirected_to symphonies_path
  end
end
